(function () {

    function Tile(description) {
        this.description = description;
    }

    function Section(description, tiles) {
        this.description = description;
        this.tiles = tiles;
    }

    angular
        .module('app')
        .component('dashboard', {
            controller: function () {
                var ctrl = this;

                ctrl.sections = [
                    new Section("Section 1", [
                        new Tile("1"),
                        new Tile("2"),
                        new Tile("3"),
                        new Tile("4")]),
                    new Section("Section 2", [
                        new Tile("5"),
                        new Tile("6"),
                        new Tile("7"),
                        new Tile("8"),
                        new Tile("9"),
                        new Tile("10")]),
                    new Section("Section 3", [
                        new Tile("11"),
                        new Tile("12")])
                ]
            },
            templateUrl: 'dashboard/templates/dashboardTemplate.html'
        });
})();