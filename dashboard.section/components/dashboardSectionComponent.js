(function () {
    angular
        .module('app')
        .component('dashboardSection', {
            bindings: {
                section: '<'
            },
            templateUrl: 'dashboard.section/templates/dashboardSectionTemplate.html'
        });
})();