(function () {
    angular
        .module('app')
        .component('dashboardTile', {
            bindings: {
                tile: "<"
            },
            templateUrl: 'dashboard.tile/templates/dashboardTileTemplate.html'
        });
})();