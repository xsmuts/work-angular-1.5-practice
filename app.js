'use strict';

(function() {
    angular.module('app', []);
})();

(function() {
    angular
        .module('app')
        .constant('API_SETTINGS', {
            'ApiUrl': 'http://frontendshowcase.azurewebsites.net/api/'
        });

    angular
        .module('app')
        .constant('SITE_SETTINGS', {
            'ListItemLimit': 5
        });
})();

(function() {
    angular
        .module('app')
        .controller('SupplierListController', SupplierListController);

    SupplierListController.$inject = ['$scope', 'SupplierService'];

    function SupplierListController($scope, supplierService) {
        $scope.filteredSuppliers = new FilteredCollection([], 0, 1);

        $scope.filterSupplierList = function(pageNumber) {
            filterSupplierList(pageNumber);
        }

        init();

        function init() {
            $scope.filterSupplierList(1);
        }

        function filterSupplierList(pageNumber) {
            supplierService
                .get(pageNumber)
                .then(refreshSupplierList)
                .catch(displayError);

            function refreshSupplierList(filteredSuppliers) {
                $scope.filteredSuppliers = filteredSuppliers;
            }

            function displayError(error) {
                alert(error.message);
            }
        }
    }
})();

(function() {
    angular
        .module('app')
        .component('pagedSupplierList', {
            controller: ['SupplierService', function(supplierService) {
                var ctrl = this;

                ctrl.suppliers = [];
                
                ctrl.filterSupplierList = function(pageNumber) {
                    filterSupplierList(pageNumber);
                }

                init();

                function init() {
                    ctrl.filterSupplierList(1);
                }

                function filterSupplierList(pageNumber) {
                    supplierService
                        .get(pageNumber)
                        .then(refreshSupplierList)
                        .catch(displayError);

                    function refreshSupplierList(filteredSuppliers) {
                        ctrl.suppliers = filteredSuppliers.data;
                    }

                    function displayError(error) {
                        alert(error.message);
                    }
                }
                
            }],
            template: `
                <h1>Suppliers</h1>
                <div class="well">
                    <ul class="list-group" ng-repeat="supplier in $ctrl.suppliers">
                        <li class="list-group-item">
                            <supplier-link supplier="supplier"></supplier-link>
                        </li>
                    </ul>
                </div>
            `
        });
})();

(function() {
    angular
        .module('app')
        .component('supplierLink', {
            template: '<a ng-href="viewSupplier/{{$ctrl.supplier.id}}">{{$ctrl.supplier.name}}</a>',
            bindings: {
                supplier: '<'
            }
        });
    
    // angular
    //     .module('app')
    //     .directive('supplierLink', supplierLink);

    // supplierLink.$inject = [];

    // function supplierLink() {
    //     return {
    //         restrict: 'E',
    //         scope: {
    //             supplier: '=',
    //             link: '='
    //         },
    //         template: '<a ng-href="viewSupplier/{{::supplier.id}}">{{::supplier.name}}</a>'
    //     }
    // }
})();

(function() {

    angular
        .module('app')
        .factory('ListSettingService', ListSettingService);

    ListSettingService.$inject = ['SITE_SETTINGS'];

    function ListSettingService(SITE_SETTINGS) {
        return {
            getListItemLimit: getListItemLimit
        }

        //////////

        function getListItemLimit() {
            return SITE_SETTINGS.ListItemLimit;
        }
    }
})();

(function() {

    angular
        .module('app')
        .factory('SupplierService', SupplierService);
    
    SupplierService.$inject = ['API_SETTINGS', '$http', 'ListSettingService'];

    function SupplierService(API_SETTINGS, $http, listSettingService) {
        return {
            get: get
        }

        //////////

        function get(pageNumber) {
            var pageSize = listSettingService.getListItemLimit();
            var numberOfRecordsSkip = (pageNumber - 1) * pageSize;
            
            return $http.get(API_SETTINGS.ApiUrl + 'Suppliers')
                .then(function (response) {
                    var results = response.data.slice(numberOfRecordsSkip, numberOfRecordsSkip + pageSize);
                    return new FilteredCollection(results, response.data.length, pageNumber);
                })
                .catch(function(error) {
                    return new RequestionException('Could not connect error');
                });
        }
    }
})();

(function() {

    angular
        .module('app')
        .directive('pagination', pagination);
    
    pagination.$inject = ['ListSettingService'];
    
    function pagination(listSettingService) {
        return {
            restrict: 'E',
            scope: {
                page: '=',
                totalItems: '=',
                maxSize: '=',
                refreshCallback: '='
            },
            template: `
                <div>
                    <ul class="pagination">
                        <li ng-class="{ active : pageNumber == page }" ng-repeat="pageNumber in pageNumbers" ng-click="refreshCallback(pageNumber)">
                            <a href="#">{{::pageNumber}}</a>
                        </li>
                    </ul>
                </div>`,
            link: function link(scope) {
                updatePageCount(scope, 0);

                scope.$watch('maxSize', function(total) {
                    updatePageCount(scope, total);
                });
            }
        }

        function updatePageCount(scope, total) {
            var pageNumbers = [];
            var pageCount = (total % listSettingService.getListItemLimit()) + 1;

            for (var i = 1; i <= pageCount; i++) {
                pageNumbers.push(i);
            }

            scope.pageNumbers = pageNumbers;
        }
    }
})();

function FilteredCollection(data, total, page) {
    this.numberOfResults = data.length;
    this.total = total;
    this.data = data;
    this.page = page;
}

function RequestionException(message) {
    this.message = message;
}