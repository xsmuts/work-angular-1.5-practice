# Angular Incubator

Angular incubator as found on the Entelect network: 
	https://confluence.entelect.co.za/display/ES/Angular+1.x+Incubator+Pack

## Requirements:
* A user should be able to view paginated list all of the system's Suppliers.
* A user should be able to create new suppliers, edit existing ones.
* A user should be prompted for confirmation before they remove a supplier from the system.
* A user should be able to easily search through all of the Suppliers.
* A user should be able to view a selection of issues, including the comic's image.
* A user should be able to place an order for issues of various grades of quality from a chosen supplier.

## API Link
ComicStock API - [http://frontendshowcase.azurewebsites.net/api](http://frontendshowcase.azurewebsites.net/api)